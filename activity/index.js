// create user profile
let firstName = "John";
let lastName = "Smith";
let age = 30;
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
let workAddress = {
    houseNumber: "32",
    street: "Washington",
    city: "Lincoln",
    state: "Nebraska"
}

// log user details
console.log(
    "First Name: " + firstName  +
    "\nLast Name: " + lastName +
    "\nAge: " + age
)
console.log("Hobbies:");
console.log(hobbies);
console.log("Work Address:");
console.log(workAddress);

// create Steve Rogers profile
let fullName = "Steve Rogers";
let currentAge = 40;
let friends = ["Tony","Bruce", "Thor", "Natasha","Clint", "Nick"];
let profile = {
    username: "captain_america",
    fullName: "Steve Rogers",
    age: 40,
    isActive: false
}

// log Captain America profile
console.log(
    "My full name is: " + fullName  +
    "\nMy current age is: " + currentAge
);
console.log("My Friends are: ");
console.log(friends);
console.log("My Full Profile: ");
console.log(profile);

// create Bucky Barnes profile
let bestFriendName = "Bucky Barnes";
const lastLocation = "Arctic Ocean";

// log Bucky Barnes details
console.log("My bestfriend is: " + bestFriendName);
console.log("I was found frozen in: " + lastLocation);
