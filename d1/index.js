console.log('Hello World');

console.log('Hello Again');

let myVar = 'This is a variable';

console.log(myVar);

let firstName = 'Andres';
let lastName = 'Bonifacio';

console.log(firstName + ' ' + lastName);

let productPrice = 18999;
const interest = 3.539;

console.log(productPrice);
console.log(interest);

// Mini-activity: Re-assigning values to declared variables. **constant variables (declared with 'const') can't be re-assigned
let friend = 'Cypher';
console.log(friend);
friend = 'Chamber';
console.log(friend);

// initializing a variable
let supplier;
// assigning value to an initialized variable
supplier = 'Raze Boom Bot Trading';
console.log(supplier)
// re-assigning value to a variable. 
supplier = 'Killjoy Hi-Tech Trading';
console.log(supplier)

// var vs let/const
// there are issues when it comes to variables declared using var, regarding hoisting.
// in terms of variables, keyword var is hoisted while let/const does not allow hoisting.
// Hoisting is Javascript's default behavior of moving declarations to the top

a = 5;
console.log(a);
var a;

// scope of variables
/*
	- scope essentially means where these variables are available for use
	- let and const variables are block scoped
	- a block is a chunk of codes bounded {}. a block lives in curly braces. anything within curly brace is a block
*/

let outerSpace = 'Solar System';
{
    let innerSpace = 'Earth';
    console.log(innerSpace);
}

console.log(outerSpace);
// console.log(innerSpace) - wouldn't work because this variable is inside a block

// Multiple variable declarations
/*
	multiple variables can be declared in one line using one keyword
	the two variable declarations must separated by a comma
	should the second variable not be changed, use separate declarations:
		let productCode = "CD017";
		const productBrand = "Dell";
	although removing keywords would use let as default, it is still advised that we use the correct keyword so that the devs would have a clue as to what type of variable has been created (or is it for reassigning of values)
*/

let productCode = 'E14152', productBrand = 'Rakk', productStore = 'EasyPC';
console.log(productCode + ', ' + productBrand + ', ' + productStore);

// trying to use a variable with a reserved keyword
/*
const let = "hello";
console.log(let); -  since let is already reserved keyword in JS, it is forbidden to use as a name of variables (would return an error)
*/

// Strings
// Strings are series of characters that create a word, a phrase, a sentence or anything related to creating a text
let country = "Philippines";
console.log(country);

// Concatenating strings
// To combine multiple variables with string values, use "+" symbol
let province = "Negros Oriental";
console.log(province + ", " + country);

// escape character - \ in strings in combination with other characters can produce different effects
// \ - back slash

// \n - would create a new line break between the text
let mailAddress = "Negros Oriental\nPhilippines";
console.log(mailAddress);

// using double quotes and single quotes for string data types are actually valid in JS
console.log("John's employees went home early.");
console.log('John\'s employees went home early.');


// Numbers
// integers/whole numbers
// with the exception of strings, other data types in javascript are color coded

let headCount = 26;
console.log(headCount);

// decimal/fractions
let grade = 98.7;
console.log(grade);

// exponential notation
let planetDistance = 2e10;
console.log(planetDistance);

// combining strings and numbers

console.log("John's grade last quarter is " + grade);

// Boolean
// are normally used to store values relating to the state of certain things
// true/false for the default value

let isMarried = false;
let inGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + inGoodConduct);

// Array
// arrays are special kind of data type that are used to store multiple values
// - can store different data types but it is normally used to store similar data types

// Syntax: let/const varName = [elementA, elementB, elementC, ..., elementN]

// similar data types
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);


// different data types
// not advisable to combine different data types in an array since it would be confusing for other devs when they read our codes
let person = ["John", "Smith", 32, true];
console.log(person);

// object data type
// objects are another special kind of data type that's used to mimic real world objects
// they are used to create complex data that contains pieces of information that are relevant to each other

// Syntax :
/* let/const varName = {
	properyA: value,
	propertyB: value
} */

let personDetails = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: true,
	contact: ["09123456789", "09987654321"],
	address: {
		houseNumber: "345",
		city: "Manila",
	}
}

console.log(personDetails);

// typeof keyword - used if the dev are not sure or wants to assure of what the data type of the variable is
console.log(typeof personDetails);

/* 
Constant Array/Objects
	the const keyword is a little bit misleading when it comes to arrays/objects

	it does not define a constant value for arrays/objects. It defines a constant reference to a value

	we CANNOT:
	Reassign a constant value
	Reassign a constant array
	Reassign a constant object

	But you CAN:
	change the elements of a constant array,
	change the properties of a constant object
*/

const anime = ["Naruto", "Slam Dunk", "One Piece"];
console.log(anime);

/* 
 would return an error because we have const variable
 anime = ["Akame ga Kill"];
*/
anime[0] = ["Akame ga Kill"];
console.log(anime);

// Null data type
let number = 0;
let string = "";
console.log(number);
console.log(string);

/* 	
  null is used to intentionally express the absence of a value inside a variable in a declaration/initialization

  one clear difference of null vs undefined; null means that the variable was created and was assigned a value that does not hold any value/amount, compared to undefined which is concerned with creating a variable but was not given any value
*/
let kabit = null;
console.log(kabit);


